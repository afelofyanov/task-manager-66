package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdRequest extends AbstractIdRequest {

    public TaskRemoveByIdRequest(@Nullable String token, @Nullable String id) {
        super(token, id);
    }
}
