package ru.tsc.felofyanov.tm.exception.entity;

public final class ModelEmptyException extends AbstractEntityNotFoundException {

    public ModelEmptyException() {
        super("Error! Model is empty...");
    }
}
