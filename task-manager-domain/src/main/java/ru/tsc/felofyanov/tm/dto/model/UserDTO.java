package ru.tsc.felofyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.listener.EntityListener;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(nullable = false, unique = true)
    private String login;

    @Nullable
    @Column(nullable = false, name = "password")
    private String passwordHash;

    @Nullable
    @Column(unique = true)
    private String email;

    @Nullable
    @Column(name = "fst_name")
    private String firstName;

    @Nullable
    @Column(name = "mid_name")
    private String middleName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false)
    private Boolean locked = false;
}

