package ru.tsc.felofyanov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.model.ITaskRepository;
import ru.tsc.felofyanov.tm.api.service.model.ITaskService;
import ru.tsc.felofyanov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Override
    protected ITaskRepository getRepository() {
        return taskRepository;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(TaskIdEmptyException::new);
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(TaskIdEmptyException::new);
        @NotNull final ITaskRepository repository = getRepository();
        repository.removeAllByProjectId(userId, projectId);
    }
}
