package ru.tsc.felofyanov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    @Nullable
    @Query("FROM Task WHERE user.id = :userId and project.id = :projectId")
    List<Task> findAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Modifying
    @Transactional
    @Query("delete FROM Task WHERE user.id = :userId and project.id = :projectId")
    void removeAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );
}
