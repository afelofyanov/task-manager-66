package ru.tsc.felofyanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.dto.IDTORepository;
import ru.tsc.felofyanov.tm.api.service.dto.IServiceDTO;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.IdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.IndexIncorrectException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IServiceDTO<M> {

    @NotNull
    protected abstract IDTORepository<M> getRepository();

    @Nullable
    protected M getResult(@NotNull final Page<M> page) {
        if (page.stream().count() == 0) return null;
        return page.getContent().get(0);
    }

    @Override
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.save(model);
    }

    @Override
    public Collection<M> add(@Nullable Collection<M> models) {
        if (models == null) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.saveAll(models);
    }

    @Override
    public M update(@Nullable M model) {
        if (model == null) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.save(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return models;
        @NotNull final IDTORepository<M> repository = getRepository();
        repository.deleteAll();
        return repository.saveAll(models);
    }

    @Override
    public void clear() {
        @NotNull final IDTORepository<M> repository = getRepository();
        repository.deleteAll();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final IDTORepository<M> repository = getRepository();
        return getResult(repository.findAll(PageRequest.of(index, 1)));
    }

    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final IDTORepository<M> repository = getRepository();
        repository.delete(model);
        return model;
    }

    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IDTORepository<M> repository = getRepository();
        @Nullable final Optional<M> result = repository.findById(id);
        if (!result.isPresent()) throw new ModelNotFoundException();
        repository.deleteById(id);
        return result.get();
    }

    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final IDTORepository<M> repository = getRepository();
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        repository.delete(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final IDTORepository<M> repository = getRepository();
        repository.deleteAll(collection);
    }

    @Override
    public long count() {
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.count();
    }
}
