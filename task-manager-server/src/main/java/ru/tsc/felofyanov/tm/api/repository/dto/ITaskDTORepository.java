package ru.tsc.felofyanov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IUserOwnerDTORepository<TaskDTO> {

    @Nullable
    @Query("FROM TaskDTO WHERE user_id = :userId and projectId = :projectId")
    List<TaskDTO> findAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Modifying
    @Transactional
    @Query("DELETE FROM TaskDTO where user_id = :userId and projectId = :projectId")
    void removeAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );
}
