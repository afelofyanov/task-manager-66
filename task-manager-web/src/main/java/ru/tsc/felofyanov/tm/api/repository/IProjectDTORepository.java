package ru.tsc.felofyanov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.felofyanov.tm.model.ProjectDTO;

public interface IProjectDTORepository extends JpaRepository<ProjectDTO, String> {

}
