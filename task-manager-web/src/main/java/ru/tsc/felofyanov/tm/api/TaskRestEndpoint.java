package ru.tsc.felofyanov.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.model.TaskDTO;

import java.util.List;

@RequestMapping("/api/tasks")
public interface TaskRestEndpoint {

    @GetMapping("/count")
    long count();

    @PostMapping("/delete")
    void delete(@RequestBody TaskDTO task);

    @PostMapping("/deleteAll")
    void clear(@RequestBody List<TaskDTO> tasks);

    @DeleteMapping("/clear")
    void clear();

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @GetMapping("/findById/{id}")
    TaskDTO findById(@PathVariable("id") String id);

    @PostMapping("/save")
    TaskDTO save(@RequestBody TaskDTO task);
}
