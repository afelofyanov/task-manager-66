package ru.tsc.felofyanov.tm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.api.ProjectRestEndpoint;
import ru.tsc.felofyanov.tm.model.ProjectDTO;
import ru.tsc.felofyanov.tm.service.ProjectDTOService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @Autowired
    private ProjectDTOService repository;

    @Override
    @GetMapping("/count")
    public long count() {
        return repository.count();
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody ProjectDTO project) {
        repository.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@RequestBody List<ProjectDTO> projects) {
        repository.remove(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        repository.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        repository.removeById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") String id) {
        return repository.existsById(id);
    }

    @Override
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() {
        return repository.findAll().stream().collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/save")
    public ProjectDTO save(@RequestBody ProjectDTO project) {
        return repository.save(project);
    }

    @Override
    @PostMapping("/saveAll")
    public List<ProjectDTO> save(@RequestBody List<ProjectDTO> projects) {
        return repository.save(projects);
    }
}
