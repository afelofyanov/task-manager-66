package ru.tsc.felofyanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.ProjectDTO;
import ru.tsc.felofyanov.tm.model.TaskDTO;
import ru.tsc.felofyanov.tm.service.ProjectDTOService;
import ru.tsc.felofyanov.tm.service.TaskDTOService;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskDTOService taskDTOService;

    @Autowired
    private ProjectDTOService projectDTOService;

    @GetMapping("/task/create")
    public String create() {
        taskDTOService.create();
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskDTOService.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") TaskDTO task, BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskDTOService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final TaskDTO task = taskDTOService.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Collection<ProjectDTO> getProjects() {
        return projectDTOService.findAll();
    }

    public Status[] getStatuses() {
        return Status.values();
    }
}
