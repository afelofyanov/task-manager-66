package ru.tsc.felofyanov.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.model.ProjectDTO;

import java.util.List;

@RequestMapping("/api/projects")
public interface ProjectRestEndpoint {

    @GetMapping("/count")
    long count();

    @PostMapping("/delete")
    void delete(@RequestBody ProjectDTO project);

    @PostMapping("/deleteAll")
    void clear(@RequestBody List<ProjectDTO> projects);

    @DeleteMapping("/clear")
    void clear();

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @GetMapping("/findById/{id}")
    ProjectDTO findById(@PathVariable("id") String id);

    @PostMapping("/save")
    ProjectDTO save(@RequestBody ProjectDTO project);

    @PostMapping("/saveAll")
    List<ProjectDTO> save(@RequestBody List<ProjectDTO> projects);
}
