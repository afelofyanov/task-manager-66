package ru.tsc.felofyanov.tm.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.felofyanov.tm.api.TaskRestEndpoint;
import ru.tsc.felofyanov.tm.model.TaskDTO;

import java.util.Arrays;
import java.util.List;

public class TaskRestEndpointClient implements TaskRestEndpoint {

    private static final String ROOT = "http://localhost:8080/api/tasks/";

    public static void main(String[] args) {
        final TaskRestEndpoint client = new TaskRestEndpointClient();
        final List<TaskDTO> tasks = client.findAll();
        System.out.println("Количество проектов: " + client.count());

        for (final TaskDTO task : tasks) {
            System.out.println("Проект: " + task.getName() + "; ID: " + task.getId());
        }

        final String id = tasks.get(0).getId();
        System.out.println("Существование проекта: " + client.existsById(id));

        final TaskDTO task = client.findById(id);
        System.out.println("Поиск проекта: " + task.getName());

        client.deleteById(id);
        System.out.println("Поиск проекта после удаления по id: " + client.existsById(id));
        System.out.println("Количество проектов: " + client.count());

        client.clear();
        System.out.println("Количество проектов после удаления всех: " + client.count());

        client.save(task);
        System.out.println("Количество проектов после сохранения: " + client.count());

        client.delete(task);
        System.out.println("Количество проектов после удаления: " + client.count());

        client.save(task);
        System.out.println("Сохранили: " + client.count());

        client.clear(tasks);
        System.out.println("Количество проектов после удаления: " + client.count());
    }

    @Override
    public long count() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "count";
        return restTemplate.getForObject(ROOT + url, Long.class);
    }

    @Override
    public void delete(TaskDTO task) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "delete";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity entity = new HttpEntity(task, headers);
        restTemplate.postForObject(ROOT + url, entity, TaskDTO.class);
    }

    @Override
    public void clear(List<TaskDTO> tasks) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "clear";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<List<TaskDTO>> entity = new HttpEntity<>(tasks, headers);
        restTemplate.postForObject(ROOT + url, entity, TaskDTO.class);
    }

    @Override
    public void clear() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "clear";
        restTemplate.delete(ROOT + url, TaskDTO.class);
    }

    @Override
    public void deleteById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "deleteById/{id}";
        restTemplate.delete(ROOT + url, id);
    }

    @Override
    public boolean existsById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "existById/{id}";
        return restTemplate.getForObject(ROOT + url, Boolean.class, id);
    }

    @Override
    public List<TaskDTO> findAll() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "findAll";
        final TaskDTO result = restTemplate.getForObject(ROOT + url, TaskDTO.class);
        return Arrays.asList(result);
    }

    @Override
    public TaskDTO findById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "findById/{id}";
        return restTemplate.getForObject(ROOT + url, TaskDTO.class, id);
    }

    @Override
    public TaskDTO save(TaskDTO task) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "save";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity entity = new HttpEntity(task, headers);
        return restTemplate.postForObject(ROOT + url, entity, TaskDTO.class);
    }
}
