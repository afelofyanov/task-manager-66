package ru.tsc.felofyanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;
import ru.tsc.felofyanov.tm.dto.request.ProjectGetByIndexRequest;
import ru.tsc.felofyanov.tm.dto.response.ProjectGetByIndexResponse;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by index.";
    }

    @Override
    @EventListener(condition = "@projectShowByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(getToken(), index);
        @NotNull final ProjectGetByIndexResponse response = getProjectEndpoint().getProjectByIndex(request);
        @Nullable final ProjectDTO project = response.getProject();
        showProject(project);
    }
}
