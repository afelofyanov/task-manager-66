package ru.tsc.felofyanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.listener.AbstractListener;

@Component
public final class HelpListener extends AbstractSystemListener {

    @Nullable
    @Autowired
    private AbstractListener[] commands;

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show terminal commands info.";
    }

    @Override
    @EventListener(condition = "@helpListener.getName() == #event.name || @helpListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Help]");
        for (AbstractListener command : commands)
            System.out.println(command);
    }
}
