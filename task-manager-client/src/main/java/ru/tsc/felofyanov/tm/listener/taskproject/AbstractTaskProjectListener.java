package ru.tsc.felofyanov.tm.listener.taskproject;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.felofyanov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractTaskProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    private IProjectTaskEndpoint projectTaskEndpoint;
}
