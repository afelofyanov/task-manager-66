package ru.tsc.felofyanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ServerAboutRequest;
import ru.tsc.felofyanov.tm.dto.response.ServerAboutResponse;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;

@Component
public final class AboutListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    @EventListener(condition = "@aboutListener.getName() == #event.name || @aboutListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[About]");
        System.out.println("[Client]");
        System.out.println("Name: " + getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getPropertyService().getAuthorEmail());
        System.out.println("\n[Server]");
        @NotNull final ServerAboutResponse response = getSystemEndpoint().getAbout(new ServerAboutRequest());
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }
}
