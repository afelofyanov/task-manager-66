package ru.tsc.felofyanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.DataSaveXmlJaxbRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;

@Component
public final class DataSaveXmlJaxbListener extends AbstractDataListener {

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in xml file(JAXB)";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataSaveXmlJaxbListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().saveDataXmlJaxb(new DataSaveXmlJaxbRequest(getToken()));
    }
}
